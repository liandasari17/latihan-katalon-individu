<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR001 - username</name>
   <tag></tag>
   <elementGuidId>69ae927d-9b3f-46f8-b30f-87552d585755</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'userName']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>userName</value>
      <webElementGuid>8bc5d0b8-0afe-475a-adcc-af117357189f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
