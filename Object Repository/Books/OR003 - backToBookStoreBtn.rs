<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR003 - backToBookStoreBtn</name>
   <tag></tag>
   <elementGuidId>b25600fb-937a-40f0-a543-e9e3f880427c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'addNewRecordButton']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>addNewRecordButton</value>
      <webElementGuid>c9848862-0e29-4ad2-999d-6d076c4cb6d3</webElementGuid>
   </webElementProperties>
</WebElementEntity>
